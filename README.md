## file-helpers

This is just a bunch of file helpers that wrap basic functionality provided by path, fs, fs-extra, os, yazl and yauzl. Documentation will hopefully come at some point.


## License

[MIT](LICENSE).
