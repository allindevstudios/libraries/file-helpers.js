// const os = require('os');
import {
  statSync,
  readdirSync,
  existsSync,
  mkdirSync,
  createWriteStream,
  writeFile,
} from 'fs';

import {
  copy,
  remove,
  removeSync,
} from 'fs-extra';
import { tmpdir } from 'os';
import { dirname, extname } from 'path';
import { ZipFile } from 'yazl';
import { open as openZipFile } from 'yauzl';
import { Parser, Builder } from 'xml2js';

/**
 * 
 * @param {string} filePath 
 */
export function isFolderSync(filePath) {
  const stats = statSync(filePath);
  return stats.isDirectory();
}

/**
 * 
 * @param {string} folderPath 
 */
export function createFolderSync(folderPath) {
  if (existsSync(folderPath)) {
    return;
  }

  const pathSegments = folderPath.split('/').slice(1);
  let currentPath = '';
  pathSegments.forEach((pathSegment) => {
    currentPath += `/${pathSegment}`;
    if (existsSync(currentPath) === false) {
      mkdirSync(currentPath);
    }
  });
}

/**
 * 
 * @param {*} filePath 
 */
export function createFilePathSync(filePath) {
  const folderPath = dirname(filePath);
  createFolderSync(folderPath);
}

/**
 * 
 * @param {string} filePath 
 */
export function deleteFileSync(filePath) {
  removeSync(filePath);
}

/**
 * 
 * @param {string} filePath 
 */
export function deleteFile(filePath) {
  return new Promise((resolve, reject) => {
    remove(filePath)
      .then(() => resolve())
      .catch(err => reject(Error(err)));
  });
}

/**
 * 
 * @param {string} filePath 
 * @param {string} currentFolder 
 * @param {string} newFolder 
 */
export function changeFolder(filePath, currentFolder, newFolder) {
  return filePath.replace(currentFolder, newFolder);
}


/**
 * 
 * @param {string} filePath 
 * @param {string} currentExtension 
 * @param {string} newExtension 
 */
export function changeExtension(filePath, currentExtension, newExtension) {
  return filePath.replace(currentExtension, newExtension);
}

/**
 * 
 * @param {string} filePath 
 */
export function removeExtension(filePath) {
  const fileExtension = extname(filePath);
  return filePath.replace(fileExtension, '');
}

/**
 * 
 * @param {string} filePath 
 * @param {string} extension 
 */
export function addExtension(filePath, extension) {
  const extensionPeriod = extension.startsWith('.') ? '' : '.';
  return `${filePath}${extensionPeriod}${extension}`;
}

/**
 * 
 * @param {string} folderName 
 */
export function makeTempFolder(folderName) {
  const folderPath = `${tmpdir()}/${folderName}`;
  createFolderSync(folderPath);
  return folderPath;
}

/**
 * 
 * @param {string} folder 
 * @param {string} extension 
 */
export function findAllFilesInFolderSync(folderPath, extension) {
  const allFilesInFolder = [];

  const folderContents = readdirSync(folderPath);
  folderContents.forEach((file) => {
    const filePath = `${folderPath}/${file}`;
    if (isFolderSync(filePath)) {
      allFilesInFolder.push(...findAllFilesInFolderSync(filePath, extension));
    } else if (!extension || extname(filePath) === extension) {
      allFilesInFolder.push(filePath);
    }
  });

  return allFilesInFolder;
}

/**
 * 
 * @param {number} ms 
 */
export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * 
 * @param {string} zippedPath 
 * @param {string} unzippedFolderPath 
 */
export function unzipToFolder(zippedPath, unzippedFolderPath) {
  const zipOptions = {
    lazyEntries: true,
  };
  return new Promise((resolve, reject) => {
    openZipFile(zippedPath, zipOptions, (unzipError, zipFile) => {
      if (unzipError) reject(Error(unzipError));
      zipFile.readEntry();
      zipFile.on('entry', (entry) => {
        if (/\/$/.test(entry.fileName)) {
          const folderPath = `${unzippedFolderPath}/${entry.fileName}`;
          createFolderSync(folderPath);
          zipFile.readEntry();
        } else {
          zipFile.openReadStream(entry, (streamError, readStream) => {
            if (streamError) reject(Error(streamError));
            const filePath = `${unzippedFolderPath}/${entry.fileName}`;
            createFolderSync(dirname(filePath));
            readStream.pipe(createWriteStream(filePath));
            readStream.on('end', () => zipFile.readEntry());
          });
        }
      });
      zipFile.on('end', () => resolve());
    });
  });
}

/**
 * 
 * @param {string} folderPath 
 * @param {string} zippedPath 
 */
export function zipFolder(folderPath, zippedPath) {
  return new Promise((resolve, reject) => {
    const zipFile = new ZipFile();
    const filesToZip = findAllFilesInFolderSync(folderPath);

    filesToZip.forEach((file) => {
      const filePathInZip = changeFolder(file, folderPath, '').replace('/', '');
      zipFile.addFile(file, filePathInZip);
    });
    zipFile.end();

    zipFile.outputStream.pipe(createWriteStream(zippedPath))
      .on('close', () => {
        resolve();
      })
      .on('error', err => reject(Error(err)));
  });
}

/**
 * 
 * @param {string} folderPath 
 * @param {string} folderCopyPath 
 */
export function copyFolder(folderPath, folderCopyPath) {
  return new Promise((resolve, reject) => {
    copy(folderPath, folderCopyPath)
      .then(() => {
        resolve();
      }).catch(err => reject(Error(err)));
  });
}

/**
 * 
 * @param {string} XML 
 */
export function XMLToJSON(XML) {
  return new Promise((resolve, reject) => {
    const parser = new Parser();
    parser.parseString(XML, (err, result) => {
      if (err) reject(Error(err));
      resolve(result);
    });
  });
}

/**
 * 
 * @param {Object} json 
 * @param {string} filePath 
 * @param {string} DTDOptions 
 */
export function JSONToXML(json, filePath, DTDOptions) {
  return new Promise((resolve, reject) => {
    const builder = new Builder(DTDOptions);
    const XML = builder.buildObject(json);
    writeFile(filePath, XML, {}, (err) => {
      if (err) reject(Error(err));
      resolve();
    });
  });
}
