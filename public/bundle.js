'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var fs = require('fs');
var fsExtra = require('fs-extra');
var os = require('os');
var path = require('path');
var yazl = require('yazl');
var yauzl = require('yauzl');
var xml2js = require('xml2js');

var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

// const os = require('os');
/**
 * 
 * @param {string} filePath 
 */
function isFolderSync(filePath) {
  var stats = fs.statSync(filePath);
  return stats.isDirectory();
}

/**
 * 
 * @param {string} folderPath 
 */
function createFolderSync(folderPath) {
  if (fs.existsSync(folderPath)) {
    return;
  }

  var pathSegments = folderPath.split('/').slice(1);
  var currentPath = '';
  pathSegments.forEach(function (pathSegment) {
    currentPath += '/' + pathSegment;
    if (fs.existsSync(currentPath) === false) {
      fs.mkdirSync(currentPath);
    }
  });
}

/**
 * 
 * @param {*} filePath 
 */
function createFilePathSync(filePath) {
  var folderPath = path.dirname(filePath);
  createFolderSync(folderPath);
}

/**
 * 
 * @param {string} filePath 
 */
function deleteFileSync(filePath) {
  fsExtra.removeSync(filePath);
}

/**
 * 
 * @param {string} filePath 
 */
function deleteFile(filePath) {
  return new Promise(function (resolve, reject) {
    fsExtra.remove(filePath).then(function () {
      return resolve();
    }).catch(function (err) {
      return reject(Error(err));
    });
  });
}

/**
 * 
 * @param {string} filePath 
 * @param {string} currentFolder 
 * @param {string} newFolder 
 */
function changeFolder(filePath, currentFolder, newFolder) {
  return filePath.replace(currentFolder, newFolder);
}

/**
 * 
 * @param {string} filePath 
 * @param {string} currentExtension 
 * @param {string} newExtension 
 */
function changeExtension(filePath, currentExtension, newExtension) {
  return filePath.replace(currentExtension, newExtension);
}

/**
 * 
 * @param {string} filePath 
 */
function removeExtension(filePath) {
  var fileExtension = path.extname(filePath);
  return filePath.replace(fileExtension, '');
}

/**
 * 
 * @param {string} filePath 
 * @param {string} extension 
 */
function addExtension(filePath, extension) {
  var extensionPeriod = extension.startsWith('.') ? '' : '.';
  return '' + filePath + extensionPeriod + extension;
}

/**
 * 
 * @param {string} folderName 
 */
function makeTempFolder(folderName) {
  var folderPath = os.tmpdir() + '/' + folderName;
  createFolderSync(folderPath);
  return folderPath;
}

/**
 * 
 * @param {string} folder 
 * @param {string} extension 
 */
function findAllFilesInFolderSync(folderPath, extension) {
  var allFilesInFolder = [];

  var folderContents = fs.readdirSync(folderPath);
  folderContents.forEach(function (file) {
    var filePath = folderPath + '/' + file;
    if (isFolderSync(filePath)) {
      allFilesInFolder.push.apply(allFilesInFolder, toConsumableArray(findAllFilesInFolderSync(filePath, extension)));
    } else if (!extension || path.extname(filePath) === extension) {
      allFilesInFolder.push(filePath);
    }
  });

  return allFilesInFolder;
}

/**
 * 
 * @param {number} ms 
 */
function sleep(ms) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, ms);
  });
}

/**
 * 
 * @param {string} zippedPath 
 * @param {string} unzippedFolderPath 
 */
function unzipToFolder(zippedPath, unzippedFolderPath) {
  var zipOptions = {
    lazyEntries: true
  };
  return new Promise(function (resolve, reject) {
    yauzl.open(zippedPath, zipOptions, function (unzipError, zipFile) {
      if (unzipError) reject(Error(unzipError));
      zipFile.readEntry();
      zipFile.on('entry', function (entry) {
        if (/\/$/.test(entry.fileName)) {
          var folderPath = unzippedFolderPath + '/' + entry.fileName;
          createFolderSync(folderPath);
          zipFile.readEntry();
        } else {
          zipFile.openReadStream(entry, function (streamError, readStream) {
            if (streamError) reject(Error(streamError));
            var filePath = unzippedFolderPath + '/' + entry.fileName;
            createFolderSync(path.dirname(filePath));
            readStream.pipe(fs.createWriteStream(filePath));
            readStream.on('end', function () {
              return zipFile.readEntry();
            });
          });
        }
      });
      zipFile.on('end', function () {
        return resolve();
      });
    });
  });
}

/**
 * 
 * @param {string} folderPath 
 * @param {string} zippedPath 
 */
function zipFolder(folderPath, zippedPath) {
  return new Promise(function (resolve, reject) {
    var zipFile = new yazl.ZipFile();
    var filesToZip = findAllFilesInFolderSync(folderPath);

    filesToZip.forEach(function (file) {
      var filePathInZip = changeFolder(file, folderPath, '').replace('/', '');
      zipFile.addFile(file, filePathInZip);
    });
    zipFile.end();

    zipFile.outputStream.pipe(fs.createWriteStream(zippedPath)).on('close', function () {
      resolve();
    }).on('error', function (err) {
      return reject(Error(err));
    });
  });
}

/**
 * 
 * @param {string} folderPath 
 * @param {string} folderCopyPath 
 */
function copyFolder(folderPath, folderCopyPath) {
  return new Promise(function (resolve, reject) {
    fsExtra.copy(folderPath, folderCopyPath).then(function () {
      resolve();
    }).catch(function (err) {
      return reject(Error(err));
    });
  });
}

/**
 * 
 * @param {string} XML 
 */
function XMLToJSON(XML) {
  return new Promise(function (resolve, reject) {
    var parser = new xml2js.Parser();
    parser.parseString(XML, function (err, result) {
      if (err) reject(Error(err));
      resolve(result);
    });
  });
}

/**
 * 
 * @param {Object} json 
 * @param {string} filePath 
 * @param {string} DTDOptions 
 */
function JSONToXML(json, filePath, DTDOptions) {
  return new Promise(function (resolve, reject) {
    var builder = new xml2js.Builder(DTDOptions);
    var XML = builder.buildObject(json);
    fs.writeFile(filePath, XML, {}, function (err) {
      if (err) reject(Error(err));
      resolve();
    });
  });
}

exports.isFolderSync = isFolderSync;
exports.createFolderSync = createFolderSync;
exports.createFilePathSync = createFilePathSync;
exports.deleteFileSync = deleteFileSync;
exports.deleteFile = deleteFile;
exports.changeFolder = changeFolder;
exports.changeExtension = changeExtension;
exports.removeExtension = removeExtension;
exports.addExtension = addExtension;
exports.makeTempFolder = makeTempFolder;
exports.findAllFilesInFolderSync = findAllFilesInFolderSync;
exports.sleep = sleep;
exports.unzipToFolder = unzipToFolder;
exports.zipFolder = zipFolder;
exports.copyFolder = copyFolder;
exports.XMLToJSON = XMLToJSON;
exports.JSONToXML = JSONToXML;
